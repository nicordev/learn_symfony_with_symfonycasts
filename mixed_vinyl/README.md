# Mixed vinyl

> https://symfonycasts.com/screencast/symfony

## flex recipes

```sh
composer recipes
composer recipes package_name_here
```

- https://github.com/symfony/recipes
- https://github.com/symfony/recipes-contrib

## debug

```sh
composer require debug
```

```php
dd($variablesHere) // dump and die
dump($variablesHere) // look in the profile to get the result
```

## routes

```sh
php bin/console debug:router
php bin/console router:match /api/songs/11 --method=POST
```

```php
    #[Route('/api/songs/{id<\d+>}', name: 'app_get_song', methods: ['GET'])]
    public function getSong(int $id): Response
```

## services

```sh
php bin/console debug:autowiring
php bin/console debug:autowiring serviceNameHere
```

## profiler

```
/_profiler
```

## webpack encore, stimulus

```sh
composer require encore
```

or for version >= 2:

```sh
composer require symfony/stimulus-bundle
```

```sh
yarn install
yarn watch
```

```sh
yarn add bootstrap --dev
yarn add @fontsource/roboto-condensed --dev
yarn add @fortawesome/fontawesome-free --dev
```

```sh
yarn add axios --dev
```

## turbo

```sh
composer require symfony/ux-turbo
# stop yarn watch then:
yarn install --force
yarn watch
```
